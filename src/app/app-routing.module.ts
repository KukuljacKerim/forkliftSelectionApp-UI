import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ManufacturerOverviewComponent } from './pages/manufacturer/manufacturer-overview/manufacturer-overview.component';
import { BrandOverviewComponent } from './pages/brand/brand-overview/brand-overview.component';
import { HomeOverviewComponent } from './home-overview/home-overview.component';
import { ManufacturerSearchComponent } from './pages/manufacturer/manufacturer-search/manufacturer-search.component';
import { CartComponent } from './pages/cart/cart.component';
import { ForkliftSearchComponent } from './pages/forklift/forklift-search/forklift-search.component';
import { ForkliftOverviewComponent } from './pages/forklift/forklift-overview/forklift-overview.component';
import { BrandSearchComponent } from './pages/brand/brand-search/brand-search.component';

const routes: Routes = [
  {
    //htttp://localhost:4200/
    path: '',
    component: HomeOverviewComponent,
  },
  {
    //htttp://localhost:4200/brand

    path: 'cart',
    component: CartComponent,
  },
  {
    //htttp://localhost:4200/brand/:brandId

    path: 'manufacturers/:manufacturerCode/overview',
    component: ManufacturerOverviewComponent,
  },
  {
    //htttp://localhost:4200/
    path: 'manufacturers/search',
    component: ManufacturerSearchComponent,
  },

  {
    //htttp://localhost:4200/brand

    path: 'brand',
    component: BrandOverviewComponent,
  }, {
    //htttp://localhost:4200/brand

    path: 'brand/search',
    component: BrandSearchComponent,
  },

  {
    //htttp://localhost:4200/
    path: 'forklifts/search',
    component: ForkliftSearchComponent,
  },
  {
    //htttp://localhost:4200/brand/:brandId

    path: 'forklift/:forkliftCode/overview',
    component: ForkliftOverviewComponent,
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
