import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { CartService } from '../../cart/services/CartService';
import { ManufacturerService } from '../shared/ManufacturerService';

@Component({
    selector: 'app-manufacturer-overview',
    templateUrl: './manufacturer-overview.component.html',
    styleUrls: ['./manufacturer-overview.component.css']
})
export class ManufacturerOverviewComponent implements OnInit {
    manufacturer: any;
    private paramMapSub!: Subscription;
    constructor(private manufacturerService: ManufacturerService, private route: ActivatedRoute, private cartService: CartService) {
        this.route.paramMap.subscribe(params => {
            const manufacturerCode = Number(params.get('manufacturerCode'));
            //da je code string
            //  const pId: number = Number(params.get('manufacturerCode'));
            this.manufacturer = this.manufacturerService.getManufacturers()
                .find(m => m.manufacturerCode === manufacturerCode);
        });
    }

    ngOnInit(): void {
    }

    public addToCart() {
        this.cartService.addToCart(this.manufacturer);
    }
    public clearCart() {
        this.cartService.clearCart();
    }

}





