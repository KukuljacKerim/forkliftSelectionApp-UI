export class Manufacturer {
    constructor(public manufacturerCode: number, public name: string, public description: string) {
    }
}