import { Injectable } from '@angular/core';
import { Manufacturer } from './manufacturer.model';

@Injectable({
    providedIn: 'root',
})
export class ManufacturerService {
    private manufacturers: Manufacturer[] = [];
    constructor() {
        this.manufacturers = [
            new Manufacturer(1, 'Sthil', 'dec1'),
            new Manufacturer(2, 'Sthil2', 'desc2'),
            new Manufacturer(3, 'Sthil3', 'desc3'),
        ];
    }

    public getManufacturers(): Manufacturer[] {
        return this.manufacturers;
    }
}
