import { Component, OnInit } from '@angular/core';
import { Manufacturer } from '../shared/manufacturer.model';
import { ManufacturerService } from '../shared/ManufacturerService';

@Component({
  selector: 'app-manufacturer-search',
  templateUrl: './manufacturer-search.component.html',
  styleUrls: ['./manufacturer-search.component.css']
})
export class ManufacturerSearchComponent implements OnInit {
  public manufacturers: Manufacturer[] = [];

  constructor(private manufacturerService: ManufacturerService) {
    this.manufacturers = this.manufacturerService.getManufacturers();
  }
  ngOnInit(): void {
  }
}



