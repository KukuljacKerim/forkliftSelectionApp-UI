import { Component, OnInit } from '@angular/core';
import { BrandService } from '../shared/brandService';

@Component({
  selector: 'app-brand-search',
  templateUrl: './brand-search.component.html',
  styleUrls: ['./brand-search.component.css']
})
export class BrandSearchComponent implements OnInit {
  brands: any;
  constructor(private service: BrandService) {
  }
  ngOnInit() {
    this.service.getBrands().subscribe((data) => {
      this.brands = data;
      console.log(data);
    });
  }

}
