import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForkliftOverviewComponent } from './forklift-overview.component';

describe('ForkliftOverviewComponent', () => {
  let component: ForkliftOverviewComponent;
  let fixture: ComponentFixture<ForkliftOverviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ForkliftOverviewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ForkliftOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
