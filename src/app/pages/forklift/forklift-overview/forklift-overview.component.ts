import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ForkliftService } from '../shared/forkliftService';
import { CartService } from '../../cart/services/CartService';

@Component({
  selector: 'app-forklift-overview',
  templateUrl: './forklift-overview.component.html',
  styleUrls: ['./forklift-overview.component.css']
})
export class ForkliftOverviewComponent implements OnInit {
  forklift: any;
  constructor(private forkliftService: ForkliftService, private route: ActivatedRoute, private cartService: CartService) {
    this.route.paramMap.subscribe(params => {
      const forkliftCode = Number(params.get('forkliftCode'));
      //da je code string
      //  const pId: number = Number(params.get('manufacturerCode'));
      this.forklift = this.forkliftService.getForklifts()
        .find(m => m.forkliftCode === forkliftCode);
    });
  }

  ngOnInit(): void {
  }

  public addToCart() {
    this.cartService.addToCart(this.forklift);
  }
  public clearCart() {
    this.cartService.clearCart();
  }

}