import { Injectable } from '@angular/core';
import { Forklift } from './forklift.model';

@Injectable({
    providedIn: 'root',
})
export class ForkliftService {
    private forklifts: Forklift[] = [];
    constructor() {
        this.forklifts = [
            new Forklift(1, 'PrviVilj', 'dec1'),
            new Forklift(2, 'DrugiVilj', 'desc2'),
            new Forklift(3, 'TreciVilj', 'desc3'),
        ];
    }

    public getForklifts(): Forklift[] {
        console.log(this.forklifts)
        return this.forklifts;
    }
}
