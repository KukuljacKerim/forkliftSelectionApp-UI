export class Forklift {
    constructor(public forkliftCode: number, public name: string, public description: string) {
    }
}