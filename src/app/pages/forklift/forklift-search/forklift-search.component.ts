import { Component, OnInit } from '@angular/core';
import { Forklift } from '../shared/forklift.model';
import { ForkliftService } from '../shared/forkliftService';

@Component({
  selector: 'app-forklift-search',
  templateUrl: './forklift-search.component.html',
  styleUrls: ['./forklift-search.component.css']
})
export class ForkliftSearchComponent implements OnInit {
  public forklifts: Forklift[] = [];

  constructor(private forkliftService: ForkliftService) {
    this.forklifts = this.forkliftService.getForklifts();
  }
  ngOnInit(): void {
  }
}