import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForkliftSearchComponent } from './forklift-search.component';

describe('ForkliftSearchComponent', () => {
  let component: ForkliftSearchComponent;
  let fixture: ComponentFixture<ForkliftSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ForkliftSearchComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ForkliftSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
