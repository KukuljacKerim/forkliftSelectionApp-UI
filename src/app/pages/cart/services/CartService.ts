import { Injectable } from '@angular/core';
import { Forklift } from '../../forklift/shared/forklift.model';

@Injectable({
    providedIn: 'root',
})
export class CartService {
    private items: Forklift[] = [];
    constructor() {
    }

    public addToCart(forklift: Forklift) {
        this.items.push(forklift);
        console.log("addToCart ", this.items);

    }
    public getItems(): Forklift[] {
        return this.items;
    }
    public clearCart(): Forklift[] {
        this.items = [];
        console.log("clearCart ", this.items);
        return this.items;
    }


}
