import { Component, OnInit } from '@angular/core';
import { CartService } from './services/CartService';
import { Forklift } from '../forklift/shared/forklift.model';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
})
export class CartComponent implements OnInit {
  public items: Forklift[] = [];
  constructor(private cartService: CartService) {
    this.items = this.cartService.getItems();
  }
  ngOnInit(): void { }

  public clearCart() {
    this.cartService.clearCart();
  }
}
