import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrandOverviewComponent } from './pages/brand/brand-overview/brand-overview.component';
import { HttpClientModule } from '@angular/common/http';
import { BrandService } from './pages/brand/shared/brandService';
import { ManufacturerOverviewComponent } from './pages/manufacturer/manufacturer-overview/manufacturer-overview.component';
import { AppRoutingModule } from './app-routing.module';
import { NavigationComponent } from './navigation/navigation.component';
import { HomeOverviewComponent } from './home-overview/home-overview.component';
import { ManufacturerSearchComponent } from './pages/manufacturer/manufacturer-search/manufacturer-search.component';
import { CartComponent } from './pages/cart/cart.component';
import { ForkliftSearchComponent } from './pages/forklift/forklift-search/forklift-search.component';
import { ForkliftOverviewComponent } from './pages/forklift/forklift-overview/forklift-overview.component';
import { BrandSearchComponent } from './pages/brand/brand-search/brand-search.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeOverviewComponent,
    NavigationComponent,
    CartComponent,

    ManufacturerOverviewComponent,
    ManufacturerSearchComponent,

    BrandOverviewComponent,
    BrandSearchComponent,

    ForkliftSearchComponent,
    ForkliftOverviewComponent,
    BrandSearchComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [BrandService],
  bootstrap: [AppComponent]
})
export class AppModule {
  title = 'Forklift Application startup';
}
